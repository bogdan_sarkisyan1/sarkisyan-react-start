const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');


gulp.task('sass', function() {
  return gulp.src('./src/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 20 versions']
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('../public'));
});

gulp.task('watch', gulp.series('sass', function(done) {
  gulp.watch('./src/**/*.scss', gulp.series('sass'));
  done();
}));