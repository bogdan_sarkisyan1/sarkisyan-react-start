import React, { useState, useEffect } from 'react';
import Button from '../Button';
import Input from '../Input';
import Form from '../Form';
import ButtonsRow from '../ButtonsRow';
import Checkbox from '../Checkbox';
import Radio from '../Radio';

function App(props) {
    const [name, useName] = useState('');
    const [surname, useSurname] = useState('');

    const handleSubmit = e => {
        e.preventDefault();
        alert(name + ' ' + surname);
    };

    return (
        <div style={{ margin: '10px' }}>
            <div
                style={{
                    width: '500px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    marginBottom: '10px',
                }}
            >
                <Button color="blue" size="def" type="def">
                    Blue
                </Button>
                <Button color="green" size="def" type="def">
                    Green
                </Button>
                <Button color="red" size="def" type="def">
                    Red
                </Button>
                <Button color="yellow" size="def" type="def">
                    Yellow
                </Button>
            </div>

            <div
                style={{
                    width: '500px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    marginBottom: '10px',
                }}
            >
                <Button color="blue" size="def" type="min">
                    Blue
                </Button>
                <Button color="green" size="def" type="min">
                    Green
                </Button>
                <Button color="red" size="def" type="min">
                    Red
                </Button>
                <Button color="yellow" size="def" type="min">
                    Yellow
                </Button>
            </div>

            <div
                style={{
                    width: '500px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    marginBottom: '10px',
                    alignItems: 'flex-end',
                }}
            >
                <Button color="blue" size="sm" type="def">
                    Blue
                </Button>
                <Button color="green" size="def" type="def">
                    Green
                </Button>
                <Button color="red" size="md" type="def">
                    Red
                </Button>
                <Button color="yellow" size="lg" type="def">
                    Yellow
                </Button>
            </div>

            <div style={{ width: '300px', marginBottom: '50px' }}>
                <Form handleSubmit={handleSubmit}>
                    <Input
                        title="Имя"
                        type="text"
                        handleChange={e => useName(e.target.value)}
                        value={name}
                        placeholder="Введи имя"
                        name="name"
                    />
                    <Input
                        title="Фамилия"
                        type="text"
                        handleChange={e => useSurname(e.target.value)}
                        value={surname}
                        placeholder="Введите фамилию"
                        name="surname"
                    />
                    <ButtonsRow pos="horizontal" rule="center">
                        <Button type="min" size="def" color="red">
                            Cancel
                        </Button>
                        <Button
                            submit={true}
                            type="def"
                            size="def"
                            color="blue"
                        >
                            Submit
                        </Button>
                    </ButtonsRow>
                </Form>
            </div>
            <div style={{ width: '300px' }}>
                <Form handleSubmit={handleSubmit} type="outer">
                    <Input
                        title="Имя"
                        type="text"
                        handleChange={e => useName(e.target.value)}
                        value={name}
                        placeholder="Введи имя"
                        name="name"
                    />
                    <Input
                        title="Фамилия"
                        type="text"
                        handleChange={e => useSurname(e.target.value)}
                        value={surname}
                        placeholder="Введите фамилию"
                        name="surname"
                    />
                    <Checkbox
                        text="Согласен справилами"
                        handleChange={e => console.log(e.target.checked)}
                    />

                    <Radio name="test1" text="Не считать" />
                    <Radio name="test1" text="2" />
                    <Radio name="test1" text="3" />
                    <ButtonsRow pos="horizontal" rule="between">
                        <Button type="min" size="def" color="blue">
                            Cancel
                        </Button>
                        <Button
                            submit={true}
                            type="def"
                            size="def"
                            color="blue"
                        >
                            Submit
                        </Button>
                    </ButtonsRow>
                </Form>
            </div>
        </div>
    );
}

export default App;
