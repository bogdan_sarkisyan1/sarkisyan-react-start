import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

function Form(props) {
    const { handleSubmit, children, type, className } = props;

    const TYPES = {
        inner: ' form--inner',
        outer: ' form--outer',
    };

    return (
        <form
            onSubmit={handleSubmit}
            className={`form${TYPES[type]} ${className}`}
        >
            {children}
        </form>
    );
}

Form.propTypes = {
    type: PropTypes.string.isRequired,
    className: PropTypes.string,
};

Form.defaultProps = {
    type: 'inner',
    className: '',
};

export default Form;
