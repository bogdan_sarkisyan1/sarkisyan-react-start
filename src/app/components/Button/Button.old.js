import React, { useState, useEffect } from 'react';

function Button(props) {
    const { children, type, color, size } = props;

    const TYPES = {
        min: 'btn--min',
        def: '',
    };

    const COLORS = {
        red: 'btn--red',
        green: 'btn--green',
        def: '',
    };

    const SIZES = {
        def: '',
        sm: 'btn--sm',
        md: 'btn--md',
        lg: 'btn--lg',
    };

    return (
        <button
            className={`btn ${TYPES[type]} ${COLORS[color]} ${SIZES[size]}`}
        >
            {children}
        </button>
    );
}

export default Button;
