import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import Icon from './../Icon';

const TYPES = {
    min: ' btn--min',
    def: '',
};

const COLORS = {
    red: ' btn--red',
    green: ' btn--green',
    yellow: ' btn--yellow',
    blue: '',
};

const SIZES = {
    def: '',
    sm: ' btn--sm',
    md: ' btn--md',
    lg: ' btn--lg',
};

function Button(props) {
    const {
        type,
        size,
        className,
        disabled,
        handleClick,
        children,
        active,
        withIcon,
        color,
        submit,
    } = props;

    //prettier-ignore
    let classNames = `btn${TYPES[type]}${SIZES[size]}${active ? ' active' : ''}${withIcon ? ' btn--with-icon' : ''}${disabled ? ' btn--disabled' : ''}${COLORS[color]}`;
    if (className && className !== '') {
        classNames += ` ${className}`;
    }

    return (
        <button
            className={classNames}
            disabled={disabled}
            onClick={handleClick}
            type={submit ? 'submit' : 'button'}
        >
            {type === 'icon' ? (
                <Fragment>
                    {withIcon && children}‌ <Icon />‌{' '}
                </Fragment>
            ) : (
                children
            )}
        </button>
    );
}

Button.propTypes = {
    type: PropTypes.oneOf(['min', 'def', 'icon']).isRequired,
    size: PropTypes.oneOf(['lg', 'md', 'sm', 'def']).isRequired,
    className: PropTypes.string,
    withIcon: PropTypes.bool,
    disabled: PropTypes.bool,
    active: PropTypes.bool,
    handleClick: PropTypes.func.isRequired,
    submit: PropTypes.bool,
};

Button.defaultProps = {
    type: 'def',
    size: 'def',
    className: '',
    disabled: false,
    active: false,
    handleClick: () => {},
    submit: false,
};

export default Button;
