import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

function Input(props) {
    const {
        handleChange,
        type,
        className,
        value,
        placeholder,
        name,
        title,
    } = props;

    return (
        <label className="input-label">
            <span className="input-title">{title}</span>
            <input
                className={`input ${className}`}
                type={type}
                onChange={handleChange}
                value={value}
                placeholder={placeholder}
                name={name}
            />
        </label>
    );
}

Input.propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    type: PropTypes.string.isRequired,
};

Input.defaultProps = {
    className: '',
    placeholder: 'Input placeholder',
    type: 'text',
};

export default Input;
