import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

function Radio(props) {
    const { text, handleChange, name, className } = props;

    return (
        <label className={`${className} radio-label`}>
            <input
                className="radio"
                type="radio"
                onChange={handleChange}
                name={name}
            />
            <span className="radio-text">{text}</span>
        </label>
    );
}

Radio.propTypes = {
    text: PropTypes.string.isRequired,
    handleChange: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    className: PropTypes.string,
};

Radio.defaultProps = {
    text: '',
    handleChange: () => {},
    name: 'radio-name',
    className: '',
};

export default Radio;
