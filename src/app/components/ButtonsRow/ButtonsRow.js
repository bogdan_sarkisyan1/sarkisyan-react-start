import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

function ButtonsRow(props) {
    const { children, pos, rule, className } = props;

    const POSITION = {
        horizontal: 'btns-row--horizontal',
        vertical: 'btns-row--vertical',
    };

    const RULES = {
        start: 'btns-row--start',
        end: 'btns-row--end',
        between: 'btns-row--between',
        center: 'btns-row--center',
    };

    return (
        <div
            className={`btns-row ${POSITION[pos]} ${RULES[rule]} ${className}`}
        >
            {children}
        </div>
    );
}

ButtonsRow.propTypes = {
    rule: PropTypes.string.isRequired,
    pos: PropTypes.string.isRequired,
    className: PropTypes.string,
};

ButtonsRow.defaultProps = {
    rule: 'between',
    pos: 'vertical',
    className: '',
};

export default ButtonsRow;
