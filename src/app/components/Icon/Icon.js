import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ICONS from './icons';

function Icon(props) {
    const { type, className } = props;
    const icon =
        ICONS[type].format === 'data' ? (
            <path d={ICONS[type].path} />
        ) : (
            ICONS[type].markup
        );

    return (
        <svg
            className={`icon ${className}`}
            viewBox={ICONS[type].viewbox}
            aria-labelledby="title"
        >
            {icon}
        </svg>
    );
}

Icon.propTypes = {
    type: PropTypes.string.isRequired,
    className: PropTypes.string,
};
Icon.defaultProps = {
    type: 'plus',
    className: '',
};

export default Icon;
