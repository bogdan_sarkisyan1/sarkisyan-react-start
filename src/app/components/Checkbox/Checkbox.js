import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

function Checkbox(props) {
    const { text, handleChange, name, className } = props;

    return (
        <label className={`${className} checkbox-label`}>
            <input
                className="checkbox"
                type="checkbox"
                onChange={handleChange}
                name={name}
            />
            <span className="checkbox-text">{text}</span>
        </label>
    );
}

Checkbox.propTypes = {
    text: PropTypes.string.isRequired,
    handleChange: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    className: PropTypes.string,
};

Checkbox.defaultProps = {
    text: '',
    handleChange: () => {},
    name: 'checkbox-name',
    className: '',
};

export default Checkbox;
